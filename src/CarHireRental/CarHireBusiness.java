/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CarHireRental;
import java.util.ArrayList; 
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author newest
 */
public class CarHireBusiness {

    /**
     * @param args the command line arguments
     */
 ///collection holding vehicle information. HashMap class which implements Map 
//interface is used. Hashmap is used because no ordering on keys or values
// is needed.
static HashMap<String, Vehicle> vehicles = new HashMap<> ();

//collection holding Bookings information. allows duplicate. 
//get() / seek operation is faster than LinkedList
static ArrayList<Bookings> bookingList = new ArrayList<> ();

// this is a collection holding the customer information.
// the map interphase is a choice because the customers' ID will be uniquely stored.
// TreeMap class implementation offers ordering by the key. Customers ID's  
// can be sorted by IDs in ascending orders.

static TreeMap<String, Customer> customers = new TreeMap<> ();

public static void initData() {
	main(null);
}
//method to get QuarterlyReport.
public static String getQuarterlyReport() {
	StringBuilder sb = new StringBuilder();
	
	Calendar c = initCalendar();
	Calendar lastQuarStart = initCalendar();
	Calendar lastQuarEnd = initCalendar();
	
	int month = c.get(Calendar.MONTH);
	if (month < 3) {
		lastQuarStart.add(Calendar.MONTH, -(month + 3)); // 10
		lastQuarStart.set(Calendar.DAY_OF_MONTH, 1);
		lastQuarEnd.add(Calendar.MONTH, -(month + 1));// 12
		lastQuarEnd.set(Calendar.DAY_OF_MONTH, 31);
	} else if (month < 6) {
		lastQuarStart.add(Calendar.MONTH, -(month)); // 1
		lastQuarStart.set(Calendar.DAY_OF_MONTH, 1);
		lastQuarEnd.add(Calendar.MONTH, -(month - 2));// 3
		lastQuarEnd.set(Calendar.DAY_OF_MONTH, 31);
	} else if (month < 9) {
		lastQuarStart.add(Calendar.MONTH, -(month - 3)); // 4
		lastQuarStart.set(Calendar.DAY_OF_MONTH, 1);
		lastQuarEnd.add(Calendar.MONTH, -(month - 5));// 6
		lastQuarEnd.set(Calendar.DAY_OF_MONTH, 30);
	} else {
		lastQuarStart.add(Calendar.MONTH, -(month - 6)); // 7
		lastQuarStart.set(Calendar.DAY_OF_MONTH, 1);
		lastQuarEnd.add(Calendar.MONTH, -(month - 8));// 9
		lastQuarEnd.set(Calendar.DAY_OF_MONTH, 30);
	}
	
	int bookingCount = 0;
	int money = 0;
	int points = 0;
	
	long lStartTime = lastQuarStart.getTimeInMillis();
	long lEndTime = lastQuarEnd.getTimeInMillis() + 24 * 60 * 60 * 1000;
	
	for (Bookings booking : bookingList) {
		
		try {
			Date dateEnd = dateFormat.parse(booking.payment.end_time);
			
			// Booking is finished within this quarter
			if (dateEnd.getTime() >= lStartTime && dateEnd.getTime() < lEndTime) {
				
				if (Bookings.PAYMENT_CUSTOMER_POINTS.equals(booking.payment.payment_type)) {
					points += booking.payment.paidPoints;
				} else {
					money += booking.payment.paidMoney;
				}
				bookingCount ++;
				
			}
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
	}
	
	sb.append(dateFormaReport.format(lastQuarStart.getTime()));
	sb.append(" -- ");
	sb.append(dateFormaReport.format(lastQuarEnd.getTime()));
	
	sb.append("    Bookings Finished:").append(bookingCount);
	sb.append(";Income(Money):").append(money);
	sb.append(";Income(Points):").append(points);
	
	
	
	return sb.toString();
}

// The method to get an available Vehicle by the type provided. 
public static Vehicle getAvailableVehicle(String type) {
	if (type == null) {
		return null;
	}
	for (Vehicle v : vehicles.values()) {
		if (type.equals(v.vehicle_type)) {
			return v;
		}
	}
	
	return null;
}

//The method to get the customer by the name provided. 
public static Customer getCustomer(String name) {
	if (name == null) {
		return null;
	}
	for (Customer c : customers.values()) {
		if (name.equals(c.name)) {
			return c;
		}
	}
	
	return null;
}

public static List<Bookings> getAllBookings() {
	return new ArrayList<Bookings>(bookingList);
}

// Method of creating new booking
// this method also provide an automatic repeat-booking facility
//of advance requirement question 1. Booking Id will be auto generated
	public static Bookings createBooking(Vehicle vehicle , Customer customer ,
	        String payment_type, String status, String start_time, String end_time) {
		
		// Create a new Booking
		Bookings newBookings = new Bookings(generateNewBookingId(), vehicle , customer,
		         payment_type,  status,  start_time,  end_time);
		
		// Add it to the collections
		bookingList.add(newBookings);
		
		System.out.println(start_time + " - " + end_time);
		
		// Return it
		return newBookings;
	}

	// Get the date of today
    public static Calendar initCalendar() {
        Calendar c = Calendar.getInstance();
        
        // Only leave year, month and date
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        
        return c;
    }

	 // Calculate how many days the vehicle is hired
	 public static int calcDaysHired(String start_time, String end_time) {
		 try {
			Date dateStart = dateFormat.parse(start_time);
			Date dateEnd = dateFormat.parse(end_time);
			
			Calendar calStart = Calendar.getInstance();
			calStart.setTime(dateStart);
			Calendar calEnd = Calendar.getInstance();
			calEnd.setTime(dateEnd);

	        // Only leave year, month and date
			calStart.set(Calendar.HOUR_OF_DAY, 0);
			calStart.set(Calendar.MINUTE, 0);
			calStart.set(Calendar.SECOND, 0);
			calStart.set(Calendar.MILLISECOND, 0);
			calEnd.set(Calendar.HOUR_OF_DAY, 0);
			calEnd.set(Calendar.MINUTE, 0);
			calEnd.set(Calendar.SECOND, 0);
			calEnd.set(Calendar.MILLISECOND, 0);
			
			return Math.abs((int) (calEnd.getTimeInMillis() - calStart.getTimeInMillis()) / (24 * 60 * 60 * 1000)) + 1;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	 }
	
    // Auto generate a new booking ID. Advanced Requirement.
	private static String generateNewBookingId() {
		String last_booking_id = null;
		if (bookingList.isEmpty()) {
			last_booking_id = "00"; // Use 0
		} else {
			last_booking_id = bookingList.get(bookingList.size() - 1).booking_id; // Use last booking object
		}

        int iNewBookingId = Integer.parseInt(last_booking_id) + 1; // create new booking id
		
        if (iNewBookingId < 10) {
    		return "0" + String.valueOf(iNewBookingId); // Append 0
        } else {
    		return String.valueOf(iNewBookingId);
        }
	}
    
    // To format the date object into string
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final SimpleDateFormat dateFormaReport = new SimpleDateFormat("yyyy-MM-dd");

    // Advanced Requirement. Create booking by days. This will create a booking for the following days
	public static void createBookingsByRepeatingDays(Vehicle vehicle , Customer customer ,
	        String payment_type, String status, int days) {
		
		Calendar calStart = initCalendar();
		Calendar calEnd = initCalendar();
		calEnd.add(Calendar.DATE, days); // add some days
		
		// create the booking by start time and end time
		createBooking(vehicle , customer ,
		        payment_type,  status, dateFormat.format(calStart.getTime()), 
		        dateFormat.format(calEnd.getTime()));
	}

    // Advanced Requirement.Create booking by weeks. This will create a booking for the following weeks
	public static void createBookingsByRepeatingWeeks(Vehicle vehicle , Customer  customer ,
	        String payment_type, String status, int weeks) {
		
		Calendar calStart = initCalendar();
		Calendar calEnd = initCalendar();
		calEnd.add(Calendar.WEEK_OF_YEAR, weeks); // add some weeks
		
		// create the booking by start time and end time
		createBooking(vehicle ,  customer ,
		        payment_type,  status, dateFormat.format(calStart.getTime()), 
		        dateFormat.format(calEnd.getTime()));
	}

    // Advanced Requirement.Create booking by months. This will create a booking for the following months
	public static void createBookingsByRepeatingMonths(Vehicle vehicle , Customer  customer ,
	        String payment_type, String status, int months) {
		
		Calendar calStart = initCalendar();
		Calendar calEnd = initCalendar();
		calEnd.add(Calendar.MONTH, months); // add some months
		
		// create the booking by start time and end time
		createBooking(vehicle ,  customer ,
		        payment_type,  status, dateFormat.format(calStart.getTime()), 
		        dateFormat.format(calEnd.getTime()));
	}

    // Advanced Requirement.Create booking by months. This will create a booking for the following months
	// For iWeekStart and iWeekEnd, use 1 - 7 which represents Sunday to Saturday
	// The booking id will be automatically generated
	public static void createBookingsByRepeatingDateForNextWeeks(Vehicle vehicle , Customer  customer ,
	        String payment_type, String status, int iWeekStart, int iWeekEnd, int weeks) {
		
		// Check the iWeekEnd and iWeekStart
		if(iWeekStart > iWeekEnd) {
			System.err.println("Week start cannot be after week end");
			return;
		}
        Calendar today = initCalendar();
        Calendar endDate = initCalendar();
        endDate.add(Calendar.WEEK_OF_YEAR, weeks - 1); // add some weeks,  this week is also included

        Calendar startDateLoop = initCalendar();
        Calendar endDateLoop = initCalendar();
        startDateLoop.set(Calendar.DAY_OF_WEEK, iWeekStart);
        endDateLoop.set(Calendar.DAY_OF_WEEK, iWeekEnd);

        // If the iWeekEnd is past in this week
        // then move to next week
        if (endDateLoop.getTimeInMillis() < today.getTimeInMillis()) {
            // Ignore this week
        } else if (startDateLoop.getTimeInMillis() < today.getTimeInMillis()) {
            // If the iWeekStart is past in this week
            // then use today as start date
    		createBooking(vehicle ,  customer ,
    		        payment_type,  status, dateFormat.format(today.getTime()), 
    		        dateFormat.format(endDateLoop.getTime()));
        } else {
    		createBooking(vehicle ,  customer ,
    		        payment_type,  status, dateFormat.format(startDateLoop.getTime()), 
    		        dateFormat.format(endDateLoop.getTime()));
        }
        
        // Move to next week
        startDateLoop.add(Calendar.WEEK_OF_MONTH, 1);
        endDateLoop.add(Calendar.WEEK_OF_MONTH, 1);
        
        long iEndDate = endDate.getTimeInMillis();
        // Loop weeks one by one
        while (startDateLoop.getTimeInMillis() <= iEndDate) {
            if (endDateLoop.getTimeInMillis() > iEndDate) {
        		createBooking(vehicle ,  customer ,
        		        payment_type,  status, dateFormat.format(startDateLoop.getTime()), 
        		        dateFormat.format(endDate.getTime()));
            } else {
        		createBooking(vehicle ,  customer ,
        		        payment_type,  status, dateFormat.format(startDateLoop.getTime()), 
        		        dateFormat.format(endDateLoop.getTime()));
            }
        
            startDateLoop.add(Calendar.WEEK_OF_MONTH, 1);
            endDateLoop.add(Calendar.WEEK_OF_MONTH, 1);
        }
	}
	
	//4b core requirement: Method of retrieving an existing booking
	public static Bookings getBooking(String booking_id) {
		// Check the input argument
		if (booking_id == null) {
			return null;
		}
		// Iterate the collections if it is not empty
		if (bookingList != null && !bookingList.isEmpty()) {
			for (Bookings booking : bookingList) {
				// Check the ID to identify the Booking
				// If found, return it
				if (booking_id.equals(booking.booking_id)) {
					return booking;
				}
			}
		}
		
		// Return null if not found
		return null;
	}
	
	// 4c core requirement: Method of indicating that a booking is in progressÂ¡Â±,
        //with the vehicle out on hire
	public static boolean setBookingInProgress(String booking_id) {
		// Check the input argument
		if (booking_id == null) {
			return false;
		}
		// Iterate the collections if it is not empty
		if (bookingList != null && !bookingList.isEmpty()) {
			for (Bookings booking : bookingList) {
				// Check the ID to identify the Booking
				// If found, update the status
				if (booking_id.equals(booking.booking_id)) {
					booking.payment.status = Bookings.STATUS_IN_PROGRESS;
					return true;
				}
			}
		}
		
		// Return null if not found
		return false;
	}
	
	//4d core requirement: Method of cancelling a booking
	public static boolean cancelBooking(String booking_id) {
		// Check the input argument
		if (booking_id == null) {
			return false;
		}
		// Iterate the collections if it is not empty
		if (bookingList != null && !bookingList.isEmpty()) {
			for (Bookings booking : bookingList) {
				// Check the ID to identify the Booking
				// If found, update the status
				if (booking_id.equals(booking.booking_id)) {
					booking.payment.status = Bookings.STATUS_CANCELLED;
					return true;
				}
			}
		}
		
		// Return null if not found
		return false;
	}
	
	//4e core requirement: Method of accepting payment for a booking
	public static boolean acceptBookingPayment(String booking_id, String payment_type) {
		// Check the input argument
		if (booking_id == null) {
			return false;
		}
		// Iterate the collections if it is not empty
		if (bookingList != null && !bookingList.isEmpty()) {
			for (Bookings booking : bookingList) {
				// Check the ID to identify the Booking
				// If found, update the payment and payment_type
				if (booking_id.equals(booking.booking_id)) {

					Customer c = customers.get(booking.customer);
					if (c == null) {
						return false;
					}
					
					if (Bookings.PAYMENT_CUSTOMER_POINTS.equals(payment_type)) { // pay by points
						if (c.points < booking.payment.paidPoints) { // Not enough points for payment
							return false;
						} else {
							// Update the points
							c.points = c.points - booking.payment.paidPoints;
						}
					}
					booking.payment.payment_type = payment_type;
					
					// Update the customer points
					c.points = c.points + booking.payment.customerPointsOwned;
					return true;
				}
			}
		}
		
		// Return null if not found
		return false;
	}
    
    
    public static void main(String[] args) {
System.out.println ("-----collections holding Vehicles information ------------");


// performing delete operation on the vehicles collection.
vehicles.clear();

// performing add operation on the vehicle collection
vehicles.put ("001", new Car("A00001", "Peugeout 206", "Blue"));
vehicles.put ("002", new Cycle("C00002", "Kawasaki V211", "Red"));
vehicles.put ("003", new Car("H00003", "Toyota Prius A1", "Silver"));

// performing find operation on the vehicle collection
vehicles.get("001");
vehicles.get("002");
vehicles.get("003");

// finding the total number of customers in the car/cycle hire business
//customers.size();
System.out.println ("Total number of vehicles are " +
vehicles.size()); 

//System.out.println ("Customer with ID1 is " +
//customers.get ("ID1")); 

// use an iterator on values
for (Vehicle vehicle : vehicles.values ()) { 
System.out.println (vehicle);
      }


System.out.println ("-----collections holding customers' information ------------");

// perrforming delete operation on the customers collection.
customers.clear();

// performing add operation on the customers collection
customers.put ("001", new Individual("Ananda", "001122", "29 Rochdale Avenue, London"));
customers.put ("002", new Business("Ramchandra", "002233", "42 Becken House, Paris"));
customers.put ("003", new Individual("Mondira", "003344", "84 Abbey Road, Hertfordshire"));

// performing find operation on the customers collection
customers.get("001");
customers.get("002");
customers.get("003");

// finding the total number of customers in the car/cycle hire business
//customers.size();
System.out.println ("Total number of customers are " +
customers.size()); 

//System.out.println ("Customer with ID1 is " +
//customers.get ("ID1")); 

// use an iterator on values
for (Customer customer : customers.values ()) { 
System.out.println (customer);
      }

System.out.println ("-----collection holding Bookings information ------------");

//perrforming delete operation on the booking collection.
bookingList.clear();

//performing add operation on the booking collection.
bookingList.add (new Bookings("01", vehicles.get("001"), customers.get("001"), "Credit Card", "Finished",
   "2014-01-02 07:30", "2014-01-02 18:00" ));
bookingList.add (new Bookings("02", vehicles.get("002"), customers.get("002"), "Cash", "Finished",
   "2014-01-03 09:30", "2014-01-03 17:00" ));
bookingList.add (new Bookings("03", vehicles.get("002"), customers.get("003"), Bookings.PAYMENT_CUSTOMER_POINTS, "Finished",
   "2014-01-04 09:30", "2014-01-08 17:00" ));
bookingList.add (new Bookings("04", vehicles.get("003"), customers.get("003"), "Cheque", "In_Progress",
   "2014-04-05 08:30", "2014-04-25 19:00" ));
bookingList.add (new Bookings("05", vehicles.get("002"), customers.get("001"), "Credit Card", "In_Progress",
   "2014-04-07 07:45", "2014-04-20 17:45" ));

//finding the total numbers of bookings
System.out.println ("Number of bookings are: " + bookingList.size());

//performing find operation on the booking collection.
bookingList.get(0);
bookingList.get(1);
bookingList.get(2);
bookingList.get(3);

//iterating through the bookings
for (int i=0; i < bookingList.size(); i++)
{
System.out.println(bookingList.get(i)); //access with get() method
}
   }

//class defining Customers. Acquiring new kind of vehicle in future can inherit from 
//this class. Inheritance is the Key forward here based on the Case Study.
 public static abstract class Customer {
     
public Customer (String name, String telephone, String address, String customertype) { 
this.name = name; 
this.telephone = telephone;
this.address = address;
this.customertype = customertype;

this.points = 100;
  }
public String toString () { 
    return name + "--"+ telephone + "--" + address + "--" + customertype;
  }
public final String name; 
public final String telephone;
public final String address;
public final String customertype;

public int points;
 
}
 
 public static class Individual extends Customer {

	public Individual(String name, String telephone, String address) {
		super(name, telephone, address, "Individual");
	}
	 
 }
 
 public static class Business extends Customer {

	public Business(String name, String telephone, String address) {
		super(name, telephone, address, "Business");
	}
	 
 }
 // this illustrates how easy to add another category of Customer
 public static class Corporate extends Customer {

	public Corporate(String name, String telephone, String address) {
		super(name, telephone, address, "Corporate");
		// TODO Auto-generated constructor stub
	}
	 
 }

//class defining Vehicles. Acquiring new kind of vehicle in future can inherit from 
//this class
 public static abstract class Vehicle {
     
public Vehicle (String vehicle_plate_no, String vehicle_type, String vehicle_brand , String vehicle_colour, 
		int price, int price_by_points, int customer_points) { 
this.vehicle_plate_no = vehicle_plate_no; 
this.vehicle_type = vehicle_type;
this.vehicle_brand = vehicle_brand;
this.vehicle_colour = vehicle_colour;
this.price = price;
this.price_by_points = price_by_points;
this.customer_points = customer_points;
}
public String toString () { 
    return vehicle_plate_no + "--"+ vehicle_type + "--" + vehicle_brand + "--" + vehicle_colour;
}
public final String vehicle_plate_no; 
public final String vehicle_type;
public final String vehicle_brand;
public final String vehicle_colour;
public final int price;
public final int price_by_points;
public final int customer_points;

 }
 
 public static class Car extends Vehicle {

public static final int CAR_PRICE = 50;
public static final int CAR_PRICE_BY_POINTS = 100; // Advanced Requirement.If pay by customer points, how many points is required
public static final int CAR_CUTOMER_POINTS = 10; // Advanced Requirement.How many points will be given to the customer if hired
	public Car(String vehicle_plate_no,
			String vehicle_brand, String vehicle_colour) {
		super(vehicle_plate_no, "Car", vehicle_brand, vehicle_colour,
				CAR_PRICE, CAR_PRICE_BY_POINTS, CAR_CUTOMER_POINTS);
	}
	 
 }
 
 public static class Cycle extends Vehicle {

public static final int CYCLE_PRICE = 5;
public static final int CYCLE_PRICE_BY_POINTS = 10; //Advanced Requirement. If pay by customer points, how many points is required
public static final int CYCLE_CUTOMER_POINTS = 1; // Advanced Requirement.How many points will be given to the customer if hired
	public Cycle(String vehicle_plate_no,
			String vehicle_brand, String vehicle_colour) {
		super(vehicle_plate_no, "Cycle", vehicle_brand, vehicle_colour,
				CYCLE_PRICE, CYCLE_PRICE_BY_POINTS, CYCLE_CUTOMER_POINTS);
	}
	 
 }
  
//class defining Bookings. Method calls makes this Class abstracted.
 public static class Bookings {
     
public Bookings (String booking_id, Vehicle vehicle , Customer customer ,
        String payment_type, String status, String start_time, String end_time) { 
this.booking_id = booking_id; 
this.vehicle = vehicle;
this.customer = customer;
this.payment = new PaymentClass(payment_type, status, start_time, end_time, 
		vehicle.price, vehicle.price_by_points, vehicle.customer_points);
}

//Call a method to return Vehicle type
public String myVehicleSubClassesType() {
	return this.vehicle.vehicle_type;
}

//Call a method to return customer name
public String myCustomerSubClassesName() {
	return this.customer.name;
}

//Call a method to return customer type
public String myCustomerSubClassesType() {
	return this.customer.customertype;
}

//Call a method to return payment
public PaymentClass myPayment() {
	return this.payment;
}

// To return details of customer like "name(type)"
// For example: Ananda (Individual), Ramchandra (Business)
public String getCustomerDetails() {
	StringBuilder sb = new StringBuilder();
	sb.append(this.customer.name);
	sb.append(" (");
	sb.append(this.customer.customertype);
	sb.append(")");
	return sb.toString();
}

//To return details of vehicle like "brand(type)
//For example: Peugeout 206 (Car), Kawasaki V211 (Cycle)
public String getVehicleDetails() {
	StringBuilder sb = new StringBuilder();
	sb.append(this.vehicle.vehicle_brand);
	sb.append(" (");
	sb.append(this.vehicle.vehicle_type);
	sb.append(")");
	return sb.toString();
}

// The status of the booking indicating it has been cancelled
public static final String STATUS_CANCELLED = "Cancelled";

// The status of the booking indicating it is â€œin progressâ€�
public static final String STATUS_IN_PROGRESS = "In_Progress";

// The payment that is paid by customer's points
public static final String PAYMENT_CUSTOMER_POINTS = "customerPoints";

public String toString () { // Paid by points

    return booking_id + "--" + vehicle.vehicle_type + "--" + customer.name + "--" + customer.customertype
    		+ "--" + payment;
}
public final String booking_id; 
public final Vehicle vehicle;
public final Customer customer;
public final PaymentClass payment;

public static class PaymentClass {
	
	public PaymentClass (String payment_type, String status, String start_time, String end_time,
			int price, int price_by_points, int customer_points) { 
		this.payment_type = payment_type;
		this.status = status;
		this.start_time = start_time;
		this.end_time = end_time;
	
		int daysHired = CarHireBusiness.calcDaysHired(start_time, end_time);
	
		paidMoney = price * daysHired;
		paidPoints = price_by_points * daysHired;
		customerPointsOwned = customer_points * daysHired;
	}
	public String payment_type;
	public String status;
	public final String start_time;
	public final String end_time;
	public final int paidMoney; // how much paid by cash or credit card
	public final int paidPoints; // how much paid by customer points
	public final int customerPointsOwned; // how much customer points will be owned by this booking
	
	public String toString() {

	    return paidPoints + "--" + payment_type + "--"
	            + status + "--" + start_time + "--" + end_time;
	}
}

 }
}
  