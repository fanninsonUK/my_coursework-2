/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CarHireRentalGui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
//import javax.swing.JTextArea;
import javax.swing.JTextField;

import CarHireRental.CarHireBusiness;
import CarHireRental.CarHireBusiness.Bookings;
import CarHireRental.CarHireBusiness.Customer;
import CarHireRental.CarHireBusiness.Vehicle;

/**
 *
 * @author newest
 */
public class CarHireRentalGUI extends JFrame {

    /**
     * @param args the command line arguments
     */
    
	/**
	 * constructor. 
	 */
	public CarHireRentalGUI(){
		initGUI();
	}
	
	private JScrollPane tableScrollPane;
	private JTable table;
	private String[] columnNames = {"booking_id",
            "customer",
            "vehicle",
            "payment",
            "payment_type",
            "status",
            "start_time",
            "end_time"
           };
	
	private void loadTable() {

		List<Bookings> allBookings = CarHireBusiness.getAllBookings();
		Object[][] data = new Object[allBookings.size()][columnNames.length];
		int idx = 0;
		for (Bookings booking : allBookings) {
			data[idx][0] = booking.booking_id;
			data[idx][1] = booking.customer.name;
			data[idx][2] = booking.vehicle.vehicle_type;
			data[idx][3] = booking.payment.paidMoney;
			data[idx][4] = booking.payment.payment_type;
			data[idx][5] = booking.payment.status;
			data[idx][6] = booking.payment.start_time;
			data[idx][7] = booking.payment.end_time;
			idx ++;
		}

		table = new JTable(data, columnNames);
		tableScrollPane.setViewportView(table);
	}
	
	private JTextField textFieldVehicle = new JTextField("");
	private JTextField textFieldCustomer = new JTextField("");
	private JTextField textFieldPaymentType = new JTextField("");
	private JTextField textFieldStatus = new JTextField(""); 
	private JTextField textFieldStartTime = new JTextField("", 10);
	private JTextField textFieldEndTime = new JTextField("");
	
	
	public void initGUI() {
		
		setTitle("");

		
		JPanel panel = new JPanel(new GridBagLayout());
		this.getContentPane().add(panel);

		tableScrollPane = new JScrollPane();
		tableScrollPane.setPreferredSize(new Dimension(800, 500));
		loadTable();		
		
		JLabel label = new JLabel("Vehicle Bookings");
		
		JPanel tableButtonPanel = new JPanel();
		//tableButtonPanel.add(new JButton("Add Thing"));
		//tableButtonPanel.add(new JButton("Delete Thing"));
		//tableButtonPanel.add(new JButton("Modify Thing"));

                //Core requirement Question 4
                //methods for managing booking.
		JPanel buttonPanel = new JPanel();
		//buttonPanel.add(new JButton("New Booking"));
		
                JButton button2 = new JButton("Retrieve Booking");
		buttonPanel.add(button2);
                //Add action listener to button2
            button2.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button2 is pressed
                 int selectedIdx = table.getSelectedRow();
                 if (selectedIdx < 0) {
                 	JOptionPane.showMessageDialog(table, "Please select a booking");
                 	return;
                 }
                 
                 String bookingId = table.getValueAt(selectedIdx, 0).toString();
                 Bookings booking = CarHireBusiness.getBooking(bookingId);
                 JOptionPane.showMessageDialog(table, booking.toString(), "Booking ID:" + bookingId, JOptionPane.PLAIN_MESSAGE);
                 
                 System.out.println("You clicked the button three");
                System.out.println("You clicked the button two");
            }
        }); 
                
                JButton button3 = new JButton("Bookings in Progress");
		buttonPanel.add(button3);
                //Add action listener to button3
            button3.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button3 is pressed
                int selectedIdx = table.getSelectedRow();
                if (selectedIdx < 0) {
                	JOptionPane.showMessageDialog(table, "Please select a booking");
                	return;
                }
                
                String bookingId = table.getValueAt(selectedIdx, 0).toString();
                CarHireBusiness.setBookingInProgress(bookingId);
                
                // Use Swing worker to update the GUI
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        loadTable();
                    }
                });
                System.out.println("You clicked the button three");
            }
        }); 
                
                JButton button4 = new JButton("Cancell a Booking");
		buttonPanel.add(button4);
                //Add action listener to button4
            button4.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button4 is pressed
	             int selectedIdx = table.getSelectedRow();
	             if (selectedIdx < 0) {
	             	JOptionPane.showMessageDialog(table, "Please select a booking");
	             	return;
	             }
	             
	             String bookingId = table.getValueAt(selectedIdx, 0).toString();
	             CarHireBusiness.cancelBooking(bookingId);
	             
	             // Use Swing worker to update the GUI
	             java.awt.EventQueue.invokeLater(new Runnable() {
	                 public void run() {
	                     loadTable();
	                 }
	             });
                System.out.println("You clicked the button four");
            }
        }); 
                
                JButton button5 = new JButton("Accepting Payment");
		buttonPanel.add(button5);
                //Add action listener to button5
            button5.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button5 is pressed

                 int selectedIdx = table.getSelectedRow();
                 if (selectedIdx < 0) {
                 	JOptionPane.showMessageDialog(table, "Please select a booking");
                 	return;
                 }
                 
                 String bookingId = table.getValueAt(selectedIdx, 0).toString();
                 
                 String paymentType = JOptionPane.showInputDialog("Please input payment type");
                 
                 if (paymentType == null || "".equals(paymentType)) {
                  	JOptionPane.showMessageDialog(table, "Not a valid payment type, please try again.");
                  	return;
                 }
                 CarHireBusiness.acceptBookingPayment(bookingId, paymentType);
                 
                 // Use Swing worker to update the GUI
                 java.awt.EventQueue.invokeLater(new Runnable() {
                     public void run() {
                         loadTable();
                     }
                 });
                System.out.println("You clicked the button five");
            }
        }); 
                
                JButton button6 = new JButton("Quarterly Reports");
		buttonPanel.add(button6);
                //Add action listener to button6
            button6.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button6 is pressed
                JOptionPane.showMessageDialog(table, CarHireBusiness.getQuarterlyReport(), 
                		"Quarterly Report", JOptionPane.PLAIN_MESSAGE);
                System.out.println("You clicked the button six");
            }
        }); 
                

		JPanel detailsPanel = new JPanel();
		detailsPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.WEST;
		panel.add(label, gbc);
		

		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0;
		gbc.gridy = 1;		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		panel.add(tableScrollPane, gbc);

		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(tableButtonPanel, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		panel.add(buttonPanel, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 2;
		gbc.anchor = GridBagConstraints.NORTH;
		
		panel.add(createDetailsPanel(), gbc);
		
		this.pack();
		
		this.setVisible(true);
	}
	
	
// add textboxes to insert booking details. 
//Booking ID will be autogenerated.
	private JPanel createDetailsPanel() {
		
		
		JPanel panel = new JPanel();
		
		JLabel anAttributeLabel = new JLabel("vehicle");
		JLabel dateFieldLabel = new JLabel("customer");
		JLabel anAttLabel = new JLabel("payment type");
		JLabel anotherAttLabel = new JLabel("status");
		JLabel anotherAtt2Label = new JLabel("start time");
        JLabel abbeyLabel = new JLabel("end time");
		
		//anotherAtt2Field.setMinimumSize(anotherAtt2Field.getPreferredSize());
		

		JCheckBox checkbox1 = new JCheckBox("A Checkbox");
		JCheckBox checkbox2 = new JCheckBox("A Checkbox");

		
		panel.setLayout(new GridBagLayout());
	
		GridBagConstraints gbc = new GridBagConstraints();
		
		int i=0;
		
		gbc.insets = new Insets(2,2,2,2);
		gbc.anchor = GridBagConstraints.NORTHEAST;
		
		gbc.gridx = 0;
		gbc.gridy = i;
		
		gbc.gridx = 1;
		gbc.gridy = i;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;		
		
		i++;
		
		//gbc.gridx = 1;
		//gbc.gridy = i;
		//gbc.gridwidth = 2;
		//panel.add(checkbox1,  gbc);

		//i++;
				
		gbc.gridx = 0;
		gbc.gridy = i;		
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(anAttributeLabel,  gbc);
				
		gbc.gridx = 1;
		gbc.gridy = i;		
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(textFieldVehicle,  gbc);		
		
		i++;
		
		gbc.gridx = 0;
		gbc.gridy = i;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(dateFieldLabel,  gbc);

		gbc.gridx = 1;
		gbc.gridy = i;		
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(textFieldCustomer,  gbc);		
		
		i++;
		
		gbc.gridx = 0;
		gbc.gridy = i;		
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(anAttLabel,  gbc);
				
		gbc.gridx = 1;
		gbc.gridy = i;				
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(textFieldPaymentType,  gbc);		

		i++;
		
		gbc.gridx = 0;
		gbc.gridy = i;		
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(anotherAttLabel,  gbc);
		
		gbc.gridx = 1;
		gbc.gridy = i;				
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
//		gbc.weightx = 1.0;
//		gbc.weighty = 1.0;
		panel.add((textFieldStatus),  gbc);

		i++;
                
		gbc.gridx = 0;
		gbc.gridy = i;		
		gbc.gridwidth = 1;
		gbc.weightx = 0.0;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(anotherAtt2Label,  gbc);

		gbc.gridx = 1;
		gbc.gridy = i;				
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		panel.add(textFieldStartTime,  gbc);
                
                i++;
                
                gbc.gridx = 0;
		gbc.gridy = i;		
		gbc.gridwidth = 1;
		gbc.weightx = 0.0;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(abbeyLabel,  gbc);

		gbc.gridx = 1;
		gbc.gridy = i;				
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		panel.add(textFieldEndTime,  gbc);
                
                //gbc.gridx = 1;
		//gbc.gridy = i;
		//gbc.gridwidth = 2;
		//panel.add(checkbox1,  gbc);

		i++;
		

		gbc.gridx = 0;
		gbc.gridy = i;
		gbc.weightx = 0;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(checkbox1,  gbc);
                
                i++;
                
                gbc.gridx = 0;
		gbc.gridy = i;
		gbc.weightx = 0;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(checkbox2,  gbc);
                
                i++;
                
                gbc.gridx = 1;
		gbc.gridy = i;
		gbc.weightx = 0;
		gbc.fill = GridBagConstraints.NONE;
                
        JButton button1 = new JButton("New Booking");
		panel.add(button1,  gbc);
                
                //Add action listener to button
            button1.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
                String text = textFieldVehicle.getText();
                if (text == null || "".equals(text)) {
                	JOptionPane.showMessageDialog(table, "Please give a vehicle");
                	return;
                }
                
                Vehicle v = CarHireBusiness.getAvailableVehicle(text);
                if (v == null) {
                	JOptionPane.showMessageDialog(table, "Cannot find an available " + text);
                	return;
                }
                
                text = textFieldCustomer.getText();
                if (text == null || "".equals(text)) {
                	JOptionPane.showMessageDialog(table, "Please give a customer name");
                	return;
                }
                Customer c = CarHireBusiness.getCustomer(text);
                if (c == null) {
                	JOptionPane.showMessageDialog(table, "Cannot find this customer");
                	return;
                }
                
                String paymentType = textFieldPaymentType.getText();
                if (paymentType == null || "".equals(paymentType)) {
                	JOptionPane.showMessageDialog(table, "Please give a payment type");
                	return;
                }
                
                String status = textFieldStatus.getText();
                if (status == null || "".equals(status)) {
                	JOptionPane.showMessageDialog(table, "Please give a status");
                	return;
                }
                
                String startTime = textFieldStartTime.getText();
                if (startTime == null || "".equals(startTime)) {
                	JOptionPane.showMessageDialog(table, "Please give a start time");
                	return;
                }
                
                String endTime = textFieldEndTime.getText();
                if (endTime == null || "".equals(endTime)) {
                	JOptionPane.showMessageDialog(table, "Please give a end time");
                	return;
                }

                CarHireBusiness.createBooking(v, c, paymentType, status, startTime, endTime);
                
                // Use Swing worker to update the GUI
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        loadTable();
                    }
                });
            }
        }); 
                //new JButton("New Booking").addActionListener(new ClickListener());
		
		return panel;
	}
    
    public static void main(String[] args) {
    	
    	// Initiate with data
    	CarHireBusiness.initData();

        // display the GUI
        CarHireRentalGUI frame = new CarHireRentalGUI();
		
		
		frame.pack();
		frame.setVisible(true);
    }
    
}
